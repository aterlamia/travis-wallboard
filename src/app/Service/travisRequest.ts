'use strict';
import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Settings} from './settings';
import 'rxjs/Rx';
import {AbstractCiRequest} from './abstractCiRequest';
// Models.
import {Build} from '../Component/Builds/build.model'
import {Commit} from '../Component/Builds/commit.model'
import {RepoSettings} from '../Component/Setting/reposettings.model'
import {Project} from '../Component/Project/project.model'
import {Observable} from "rxjs/Observable";

@Injectable()

export class TravisRequest extends AbstractCiRequest {
    constructor(http: Http, settings: Settings) {
        super(http, settings);
    }

    /**
     * Load project data from travis.
     *
     * @param {RepoSettings} settings
     * @returns {Promise<any>}
     */
    loadProjectData(settings: RepoSettings) {
        let uri;
        if (settings.isPrivate()) {
            uri = this.settings.getUri(settings) + 'repos' + '?active=true';
        } else {
            uri = this.settings.getUri(settings) + 'repos/' + encodeURI(settings.getName()) + '?active=true';
        }

        let headers = new Headers({
            'Accept': 'application/vnd.travis-ci.2+json',
            'Authorization': this.settings.getToken(settings.isPrivate(), settings.getToken())
        });
        let options = new RequestOptions({headers: headers});

        let promise = new Promise((resolve, reject) => {
            this.http.get(uri, options).toPromise().then(res => {
                    let repos = res.json().repos;
                    let repositories = [];

                    for (let repo of repos) {
                        if (repo.active) {
                            let projectModel = new Project(
                                repo.id,
                                repo.slug,
                                repo.active,
                                repo.description,
                                repo.last_build_started_at,
                                repo.last_build_finished_at,
                                repo.last_build_id,
                                repo.last_build_number,
                                repo.last_build_state
                            );
                            repositories.push(projectModel);
                        }
                    }
                    resolve(repositories);
                },
                msg => { // Error
                    reject(msg);
                });
        });
        return promise;
    }

    /**
     * Get a travis access token from a git hub token.
     *
     * @param {string} gitHubToken
     * @returns {Observable<R>}
     */
    getTravisToken(gitHubToken: string) {
        let uri = "https://api.travis-ci.com/auth/github?github_token=" + gitHubToken;

        let headers = new Headers({'Accept': 'application/vnd.travis-ci.2+json'});
        let options = new RequestOptions({headers: headers});

        let params = JSON.stringify({github_token: gitHubToken});
        return this.http.post(uri, params, options).map(res => res.json());
    }

    /**
     * Retrieve a single travis build.
     *
     * @param {RepoSettings} settings
     * @param {string} slug
     * @param id
     * @param {Number} buildId
     * @returns {Observable<R>}
     */
    getBuild(settings: RepoSettings, slug: string, id: number, buildId) {
        let uri = this.settings.getUri(settings) + 'repos/' + encodeURI(slug) + "/builds/" + buildId;

        let headers = new Headers({
            'Accept': 'application/vnd.travis-ci.2+json',
            'Authorization': this.settings.getToken(settings.isPrivate(), settings.getToken())
        });
        let options = new RequestOptions({headers: headers});

        return new Promise((resolve, reject) => {
            this.http.get(uri, options).toPromise().then(res => {
                resolve(this.createBuildModel(res.json().build, res.json().commit, slug));
            });
        });
    }

    /**
     *
     * @param {RepoSettings} settings
     * @param {string} slug
     *
     * @param all
     * @returns {Observable<R>}
     */
    getBuilds(settings: RepoSettings, slug: string, id: number, all: boolean) {
        if (typeof all === 'undefined') {
            all = false;
        }
        let uri = this.settings.getUri(settings) + 'repos/' + encodeURI(settings.getName()) + "/" + encodeURI(slug) + "/builds";

        if (all == false) {
            uri += '?event_type[0]=cron&event_type[1]=push';
        }
        let headers = new Headers({
            'Accept': 'application/vnd.travis-ci.2+json',
            'Authorization': this.settings.getToken(settings.isPrivate(), settings.getToken())
        });
        let options = new RequestOptions({headers: headers});

        let promise = new Promise((resolve, reject) => {
            this.http.get(uri, options).toPromise().then(res => {
                    let buildsData = res.json().builds;
                    let commitsData = res.json().commits;
                    let builds = [];

                    for (let index in buildsData) {
                        let build = this.createBuildModel(buildsData[index], commitsData[index], slug);
                        builds.push(build);
                    }
                    resolve(builds);
                }, msg => { // Error
                    reject(msg);
                }
            )
        });
        return promise;
    }

    /**
     * Create a build mode from the retreived data.
     *
     * @param {Object} data
     * @param {Object} commit
     *
     * @returns {Build} Build object.
     */
    createBuildModel(data, commit, slug) {
        let commitModel = new Commit(
            commit.id,
            commit.branch,
            commit.author_email,
            commit.author_name,
            commit.message,
            commit.pull_request_number
        );
        return new Build(
            data.id,
            data.number,
            slug,
            data.state,
            data.started_at,
            data.finished_at,
            data.pull_request_number !== null,
            data.pull_request_number,
            data.event_type,
            commitModel
        );
    }

    handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
