'use strict';

import {Component, Output, Input} from '@angular/core';

// COMPONENTS
import {StatusSuccessComponent} from './status.success.component';

// TEMPLATE
@Component({
    selector: 'pending-item',
    templateUrl: './status.pending.html',
})

export class StatusPendingComponent extends StatusSuccessComponent {
    @Input()
    build;
    pos;

}
