'use strict';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'timeAgo'})
export class TimeAgo implements PipeTransform {
    transform(dateString, args) {
        let dt = new Date(Date.parse(dateString));
        let now = new Date();

        let diff = now.getTime() - dt.getTime();

        let minutes = Math.floor((diff / (60000)));

        if ( isNaN(minutes) ) {
            return 1 + ' Seconds';
        } else if ( minutes === 0 ) {
            let seconds = Math.floor((diff / (1000)));
            return seconds + ' Seconds';
        } else if ( minutes > 1440 ) {
            return Math.floor((diff / 86400000)) + ' Days';
        } else if ( minutes > 60 ) {
            let hours = Math.floor((diff / 3600000));
            minutes = Math.floor(((diff % 3600000) / 60000));
            return hours + ' Hours and ' + minutes + ' Minutes';
        } else {
            return minutes + ' Minutes';
        }
    }
}


