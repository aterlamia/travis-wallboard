'use strict';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'stateClass'})
export class StateClass implements PipeTransform {
    transform(state, args) {
        let blockClass = '';
        if (state === 'failed' || state === 'Failed') {
            blockClass = 'build-failed-item';
        } else if (state === 'passed' || state === 'finished' || state === 'Successful' || state === 'success') {
            blockClass = 'build-success-item';
        } else if (state === 'started' || state === 'received' || state === 'created' || state === 'running') {
            blockClass = 'build-pending-item';
        } else if (state === 'canceled') {
            blockClass = 'build-canceled-item';
        } else if (state === 'manual') {
          blockClass = 'build-manual-item';
        }else {
            blockClass = 'build-error-item';
        }

        return blockClass;
    }
}
