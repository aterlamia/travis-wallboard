'use strict';
import {ActivatedRoute} from "@angular/router";

declare let $: any;

import {Component, ChangeDetectorRef} from '@angular/core';
import {Store} from "../../Service/store";
import {CiRequestFactory} from "../../Service/ciRequestFactory";

import {OnDestroy, OnInit} from '@angular/core';
import {Build} from "../Builds/build.model";
import {RepoSettings} from "../Setting/reposettings.model";
import {ReposService} from "../Repos/repos.service";

@Component({
  templateUrl: './project.component.html'
})

export class ProjectComponent implements OnDestroy, OnInit {
  private builds = {};
  public successBuildsArray = [];
  private timer;
  private interval: number = 1000;
  private store: Store;
  private requestFactory: CiRequestFactory;
  private reposService: ReposService;
  private cdr: ChangeDetectorRef;
  private route: ActivatedRoute;
  private project: string;
  private repo: string;
  private isPrivate: boolean;
  private repoSetting: RepoSettings;

  constructor(route: ActivatedRoute, store: Store, requestFactory: CiRequestFactory, reposService: ReposService, cdr: ChangeDetectorRef) {
    this.store = store;
    this.requestFactory = requestFactory;
    this.reposService = reposService;
    this.cdr = cdr;
    this.route = route;
  }

  /**
   * Initializes builds and repos.
   */
  init(first: boolean) {
    if (first === true || this.repoSetting.shouldUpdate(this.interval)) {
      let request = this.requestFactory.create('travis');
      let resource = request.getBuilds(
        this.repoSetting,
        this.project,
        1, //??????
        true
      );
      resource.then(builds => this.setBuilds(builds));
    }
  }

  isEmpty(val) {
    return (val === "" || typeof(val) === "undefined" || val === null);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.project = params['project']; // (+) converts string 'id' to a number
      this.isPrivate = typeof(params['private']) !== 'undefined';

      this.repo = params['repo']; // (+) converts string 'id' to a number
      this.repoSetting = this.store.getRepoSetting(this.repo, this.isPrivate);
    });

    this.init(true);
    this.timer = setInterval(() => {
      this.init(false);
      this.adjustToScreenHeight()

    }, this.interval);

    console.info('Project COMPONENT: Added interval');
  }


  adjustToScreenHeight() {
    // Try to set opimal height to show 4 rows on screen
    // 745 Is height of rows 2,3,4 inclusive margines.
    let firstRowHeight = $(window).height() - 805;
    $('.first-row .ablock').height(Math.max(firstRowHeight, 280));
  }


  ngOnDestroy() {
    clearInterval(this.timer);
    console.info('PROJECT COMPONENT: Removed interval');
  }

  setBuilds(builds) {
    let index = 1;
    this.builds = {};
    this.successBuildsArray = [];
    builds.forEach((build: Build) => {
        if (typeof this.builds[build.getCommit().getBranch() + build.getPrnr()] === 'undefined') {
          this.builds[build.getCommit().getBranch() + build.getPrnr()] = {key: build.getNumber(), val: build};
          this.successBuildsArray.push({key: build.getNumber(), val: build, index: index++});
        }
      }, this
    );

    this.cdr.detectChanges();
  }
}
